
import { readFile } from 'fs';
import { createServer } from 'http';
import { parse } from 'querystring';

var clients = [];
var messages = [];

async function sendAllMessegesEvents(messages, client) {
	messages.forEach( function (message_send) {
		//console.log('Message Event:%s=>%s', client.clientAddress, message_send);	  
		client.respond.write(message_send);
	});	
}

async function sendEventsToAll(message) {
	clients.forEach( function(client) {
		var state = client.respond.write(message);		
		
		// remove from list if node does not exist
		if (state == 0)
		{
			clients = clients.filter( function(client_enum) {
				return client_enum !== client;
			});
		}
		else
		{
			console.log('Sent to %s', client.clientAddress);
		}
	});
}

function getParser(respond, request, get) {

    var clientAddress = request.connection.remoteAddress;

	if (request.url ==='/event') {
	    respond.writeHead(200, {
	    	'Content-Type': 'text/event-stream',
	    	'Cache-Control': 'no-cache',
	    	'Connection': 'keep-alive',
		});
	  
	    var newClient = {
	    	    respond: respond,
	    	    clientAddress: clientAddress,
	    };
		request.on('close', function() {
			console.log("Connection closed: %s", clientAddress);
		});

		// add client to event list
	    clients.push(newClient);		
		sendAllMessegesEvents(messages, newClient);
	    return;
    }    
	if (get.json) {
		// from here sever can send configuration data
		//respond.writeHead(200, {'Content-Type': 'text/html'});
	    respond.end();
    	var message = get.json;
    	if (messages.push(message) > 50) {
    		messages.shift();
    	}
    	sendEventsToAll(message);
    	var data = JSON.parse(message.substring(6)); // remove "event" from begin string`
		if (data.type == 'text') {
			console.log('JSON data from: %s event:%s', clientAddress, data.type);
		} else if (data.type == 'chart') {
			console.log('JSON data from: %s event:%s:%s', clientAddress, data.type, data.name);				
		}		  
    	return;
	}
		
	var file = request.url.substring(1);
	if (file === '') { file = 'index.html'; }
	console.log('get URL:%s', file);
	
    readFile(file, function( error, content ){
    	if (error) {
    	    respond.writeHead(200, {'Content-Type': 'text/html'});
            respond.end('File not exist');    		
    		return console.log(error);
    	}
    	if (file.split('.').pop()==='js') {
		    respond.writeHead(200, {'Content-Type': 'application/x-javascript'});
	    }else{
   	    	respond.writeHead(200, {'Content-Type': 'text/html'});
    	}
        respond.write(content);
    	respond.end();
    });
}

function postParser(respond, post) {
	respond.writeHead(200, {'Content-Type': 'text/html'});
    if (post.message) {
        respond.end('OK');
    	return console.log('Post message request: %s', post.message);
    }
}

var server = createServer(function handler(request, respond) {

    var queryData = "";
        
	if (request.method === 'POST') {
		request.on('data', function (data) {
			queryData += data;
		});
        
		request.on('end', function() {    	
			var post = parse(queryData);
			postParser(respond, post);  
		});
		return;
	} 
	if (request.method === 'GET') {
		request.on('data', function (data) {
			queryData += data;
		});
        
		request.on('end', function() {
			var get = parse(queryData);
			getParser(respond, request, get);  
		});		
		return;
	}
	console.log('Unsupported:%s', request);
});

server.listen(80);

server.on('listening', function()  {
	console.log('Server running at http://%s:%d\n', server.address().address, server.address().port);
});

