/*
 * bme280-interface.h
 *
 *  Created on: 2 lis 2020
 *      Author: chris
 */

#ifndef MAIN_BME280_INTERFACE_H_
#define MAIN_BME280_INTERFACE_H_

#include "../BME280_driver-master/bme280.h"

void bme280_setup(void);
void bme280_measure(struct bme280_data* bme280_data);
void bme280_printData(struct bme280_data* bme280_data);


#endif /* MAIN_BME280_INTERFACE_H_ */
