/*
 * bme280-interface.c
 *
 *  Created on: 2 lis 2020
 *      Author: chris
 */

#include "driver/i2c.h"

#include "../BME280_driver-master/bme280_defs.h"
#include "../BME280_driver-master/bme280.h"
#include "time.h"

#define BME280_SDA_PIN 						GPIO_NUM_23
#define BME280_SCL_PIN 						GPIO_NUM_22
#define BME280_I2C_INSTANCE 				I2C_NUM_1

static int8_t i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr);
static int8_t i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr);
static void delay_us(uint32_t usek, void *intf_ptr);
static void bme280_interface_init(struct bme280_dev* bme);


struct bme280_dev bme_h;

void bme280_setup(void)
{
    // Setup I2C interface
    i2c_config_t conf = {0};

    conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = BME280_SDA_PIN;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_io_num = BME280_SCL_PIN;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = 100000;
	conf.clk_flags = 0;
	i2c_param_config(BME280_I2C_INSTANCE, &conf);
	i2c_driver_install(BME280_I2C_INSTANCE, conf.mode, 0, 0, 0);

    // Initialize communication with the sensor
	uint8_t data;
	uint8_t addr = BME280_I2C_ADDR_PRIM;
	i2c_read(0xD0, &data, 1, &addr);
    printf("BME280 id:0x%X found on address:0x%X\n", data, BME280_I2C_ADDR_PRIM);

	bme280_interface_init(&bme_h);
	bme280_init(&bme_h);
	/* Recommended mode of operation: Indoor navigation */
	uint8_t settings_sel;

	bme_h.settings.osr_h = BME280_OVERSAMPLING_1X;
	bme_h.settings.osr_p = BME280_OVERSAMPLING_16X;
	bme_h.settings.osr_t = BME280_OVERSAMPLING_2X;
	bme_h.settings.filter = BME280_FILTER_COEFF_16;

	settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
	bme280_set_sensor_settings(settings_sel, &bme_h);
}

void bme280_measure(struct bme280_data* bme280_data)
{
	bme280_set_sensor_mode(BME280_FORCED_MODE, &bme_h);

	/* Wait for the measurement to complete and print data @25Hz */
	delay_us(40000, NULL);

	bme280_get_sensor_data(BME280_ALL, bme280_data, &bme_h);
}

void bme280_printData(struct bme280_data* bme280_data)
{
	//////////////////////////////// print data ///////////////////////////////////
	float t = bme280_data->temperature;
	float p = 0.01 * bme280_data->pressure;
	float h = bme280_data->humidity;
	printf("BME280: Temp:%.2f Pres:%.2f Humi:%.2f\n", t,p,h);
	printf("----------------------------------------\n");
}

static int8_t i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr)
{
	esp_err_t ret;
	i2c_cmd_handle_t command_h = i2c_cmd_link_create();

	i2c_master_start(command_h);
	i2c_master_write_byte(command_h, (*(uint8_t*)intf_ptr << 1) | I2C_MASTER_WRITE, true);

	i2c_master_write_byte(command_h, reg_addr, true);
	i2c_master_write(command_h, (uint8_t*)data, len, true);
	i2c_master_stop(command_h);

	ret = i2c_master_cmd_begin(BME280_I2C_INSTANCE, command_h, 10/portTICK_PERIOD_MS);
	i2c_cmd_link_delete(command_h);

	return (int8_t)ret;
}

static int8_t i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr)
{
	esp_err_t ret;

	i2c_cmd_handle_t command_h = i2c_cmd_link_create();

	i2c_master_start(command_h);
	i2c_master_write_byte(command_h, (*(uint8_t*)intf_ptr << 1) | I2C_MASTER_WRITE, true);
	i2c_master_write_byte(command_h, reg_addr, true);

	i2c_master_start(command_h);
	i2c_master_write_byte(command_h, (*(uint8_t*)intf_ptr << 1) | I2C_MASTER_READ, true);

	if (len > 1) {
		i2c_master_read(command_h, data, len-1, I2C_MASTER_ACK);
	}
	i2c_master_read_byte(command_h, data+len-1, I2C_MASTER_NACK);
	i2c_master_stop(command_h);

	ret = i2c_master_cmd_begin(BME280_I2C_INSTANCE, command_h, 10/portTICK_PERIOD_MS);
	i2c_cmd_link_delete(command_h);

	return (int8_t)ret;
}

static void delay_us(uint32_t usek, void *intf_ptr)
{
	vTaskDelay( usek / portTICK_PERIOD_MS / 1000);
}

static void bme280_interface_init(struct bme280_dev* bme)
{
	static const uint8_t i2c_address = BME280_I2C_ADDR_PRIM;

	/* Map the delay function pointer with the function responsible for implementing the delay */
	bme->delay_us = delay_us;

	/* Assign device I2C address based on the status of SDO pin (GND for PRIMARY(0x76) & VDD for SECONDARY(0x77)) */
	bme->intf_ptr = (void *)&i2c_address;

	/* Select the interface mode as I2C */
	bme->intf = BME280_I2C_INTF;

	/* Map the I2C read & write function pointer with the functions responsible for I2C bus transfer */
	bme->read = i2c_read;
	bme->write = i2c_write;
}

