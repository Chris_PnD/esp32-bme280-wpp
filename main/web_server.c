/*
 * web_server.c
 *
 *  Created on: 3 lis 2020
 *      Author: chris
 */
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/timers.h"
#include "esp_http_server.h"
#include "esp_http_client.h"
#include "esp_err.h"
#include "esp_netif.h"
//#include "tcpip_adapter.h"

#include "web_server.h"

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// macros /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX_EVENT_SOCKET 5
#define SIZE_OF_SEND_QUEUE 40

#define DATA_SERVER_ADDRESS 	"http://192.168.0.100"

// header of query for data server
#define JSON_DATA_SERVER_HEADER				"json"
////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// type definition ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
    httpd_handle_t hd;
    int fd;
} async_resp_arg_t;

typedef struct event_queue{
	web_cmd_t type;
	uint64_t time_ms;
	float y;
} event_queue_t;

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// static function prototypes //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
static const char *web_cmd_name[] = {"Time", "Temperature", "Pressure", "Humidity"};
static esp_err_t get_handler(httpd_req_t *req);
static esp_err_t post_handler(httpd_req_t *req);
static esp_err_t event_handler(httpd_req_t *req);
static esp_err_t setup_handler(httpd_req_t *req);
static void close_socket(httpd_handle_t hd, int sockfd);
static void sending_data_task(void* param);
static void send_data_to_server(event_queue_t* event_queue_pt);
static void setup_dev(const char* setup);
////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// external function ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

int httpd_default_send(httpd_handle_t hd, int sockfd, const char *buf, size_t buf_len, int flags);

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// variables //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
extern const char main_start[] 	asm("_binary_main_js_start");
extern const char main_end[] 	asm("_binary_main_js_end");
extern const char index_start[] asm("_binary_index_html_start");
extern const char index_end[]   asm("_binary_index_html_end");

static TaskHandle_t send_task_h;
static QueueHandle_t event_queue_h;
static async_resp_arg_t resp_arg[MAX_EVENT_SOCKET];

uint8_t chart_bitfield = (1 << TEMP) | (1 << PRES) | (1 << HUMI);

const char hdr[] = "HTTP/1.1 200 OK\nContent-Type: text/event-stream\nCache-Control: no-cache\n\n\n";

/* URI handler structure for GET / */
httpd_uri_t uri_get = {
    .uri      = "/*",
    .method   = HTTP_GET,
    .handler  = get_handler,
    .user_ctx = NULL
};

/* URI handler structure for POST / */
httpd_uri_t uri_post = {
    .uri      = "/*",
    .method   = HTTP_POST,
    .handler  = post_handler,
    .user_ctx = NULL
};

httpd_uri_t event = {
    .uri       = "/event*",
    .method    = HTTP_GET,
    .handler   = event_handler,
    .user_ctx  = NULL
};

httpd_uri_t setup = {
    .uri       = "/setup*",
    .method    = HTTP_GET,
    .handler   = setup_handler,
    .user_ctx  = NULL
};

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// global function ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

void web_server_send_event(web_cmd_t i, uint64_t time_ms, float y)
{
	event_queue_t event_queue;
	event_queue.time_ms = time_ms;
	event_queue.y = y;
	event_queue.type = i;
    if ( event_queue_h != NULL )
    {
    	// if queue full drop last message
    	if (uxQueueSpacesAvailable(event_queue_h) == 0)
    	{
    		event_queue_t event_drop_queue;
    		xQueueReceive(event_queue_h, &event_drop_queue, ( TickType_t ) 10);
    	}
    	// try to send message to end of queue
        if ( xQueueGenericSend( event_queue_h, ( void * ) &event_queue, ( TickType_t ) 10, queueSEND_TO_BACK ) != pdPASS )
        {
            printf("Failed to post the message, even after 10 ticks\n");
        }
    }
    // send data to data server
    send_data_to_server(&event_queue);
}

/* Function for starting the webserver */
httpd_handle_t start_webserver(void)
{
	// reset all slot - connection socket table
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		resp_arg[i].fd = -1;
	}

    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn = httpd_uri_match_wildcard;
    config.close_fn = close_socket;

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &event);
        httpd_register_uri_handler(server, &setup);
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_post);

        // Create queue to store sending data
        event_queue_h = xQueueCreate(SIZE_OF_SEND_QUEUE, sizeof(event_queue_t));

        /* Create the sending data task, storing the handle. */
        xTaskCreate(sending_data_task,	/* Function that implements the task. */
        			"send-task",		/* Text name for the task. */
					2048,      			/* Stack size in words, not bytes. */
					NULL,		    	/* Parameter passed into the task. */
					tskIDLE_PRIORITY,	/* Priority at which the task is created. */
					&send_task_h );		/* Used to pass out the created task's handle. */
    }
    /* If server failed to start, handle will be NULL */
    return server;
}

/* Function for stopping the webserver */
void stop_webserver(httpd_handle_t server)
{
    if (server) {
        /* Stop the httpd server */
        httpd_stop(server);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// static function ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
        	printf("HTTP_EVENT_ERROR\n");
            break;
        case HTTP_EVENT_ON_CONNECTED:
//        	printf("HTTP_EVENT_ON_CONNECTED\n");
            break;
        case HTTP_EVENT_HEADER_SENT:
//        	printf("HTTP_EVENT_HEADER_SENT\n");
            break;
        case HTTP_EVENT_ON_HEADER:
//        	printf("HTTP_EVENT_ON_HEADER\n");
//            printf("%.*s\n", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
//        	printf("HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
//            if (!esp_http_client_is_chunked_response(evt->client)) {
//                printf("%.*s\n", evt->data_len, (char*)evt->data);
//            }
            break;
        case HTTP_EVENT_ON_FINISH:
//        	printf( "HTTP_EVENT_ON_FINISH\n");
            break;
        case HTTP_EVENT_DISCONNECTED:
//        	printf("HTTP_EVENT_DISCONNECTED\n");
            break;
        case HTTP_EVENT_REDIRECT:
        	break;
    }
    return ESP_OK;
}

static void send_data_to_server(event_queue_t* event_queue_pt)
{
	char buf_out[100];
	char buf_in[50] = {0};
	char staIpString[20];

    esp_netif_t *netif = NULL;
    esp_netif_ip_info_t ip;
    netif = esp_netif_next_unsafe(netif);
    esp_netif_get_ip_info(netif, &ip);
	sprintf(staIpString, IPSTR, IP2STR(&ip.ip));

	// one line JSON format
	sprintf(buf_out,"%s=data: {\"type\": \"chart\", \"name\": \"%s-%s\", \"x\": %llu, \"y\": %.3f}\n\n",
			JSON_DATA_SERVER_HEADER, staIpString, web_cmd_name[event_queue_pt->type], event_queue_pt->time_ms, event_queue_pt->y);

	esp_http_client_config_t config = {
	   .url = DATA_SERVER_ADDRESS,
	   .event_handler = _http_event_handle,
       .user_data = buf_in,
	};
	esp_http_client_handle_t client = esp_http_client_init(&config);
	//esp_http_client_set_header(client, "Content-Type", "application/json");
	esp_http_client_set_post_field(client, buf_out, strlen(buf_out));
	esp_err_t err = esp_http_client_perform(client);

	if (err == ESP_OK) {
		int content_length = esp_http_client_get_content_length(client);
//		printf("Status = %d, content_length = %d\n",
//	        esp_http_client_get_status_code(client), content_length);
		if ((content_length < 0) || (content_length > sizeof(buf_in)))
			content_length = sizeof(buf_in);
		if (content_length > 0)
		{
			esp_http_client_read(client, buf_in, content_length);

			if (memcmp(buf_in, "No config", 9) != 0) {
				printf("Server answer: %s, size:%d\n", buf_in, content_length);
				setup_dev(buf_in);
			}
		}
	}
	esp_http_client_cleanup(client);
}

static void sending_data_task(void* param)
{
    // Suspend task - wait for connection socket
	vTaskSuspend(NULL);

	while(1)
	{
		event_queue_t event_queue;
		if( xQueueReceive( event_queue_h, &event_queue, portMAX_DELAY ) )
		{
			char buf[100];

			// one line JSON format
			sprintf(buf,"data: {\"type\": %d, \"x\": %llu, \"y\": %.3f}\n\n",
								event_queue.type, event_queue.time_ms, event_queue.y);
			// send data to all sockets
			for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
			{
				if (resp_arg[i].fd >= 0)
				{
					httpd_default_send(resp_arg[i].hd, resp_arg[i].fd, buf, strlen(buf), 0);
					printf("Asynchronous send data\n");
				}
			}
		}
	}
}

static void close_socket(httpd_handle_t hd, int sockfd)
{
	// find socket to close it
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (sockfd == resp_arg[i].fd)
		{
			printf("Close socket fd:%d\n", sockfd);
			resp_arg[i].fd = -1;
		}
	}
	// if not active socket stop send
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (resp_arg[i].fd > 0)
		{
			return;
		}
	}
	vTaskSuspend(send_task_h);
}

static void add_socketfd_to_table(int fd, httpd_req_t *req)
{
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (resp_arg[i].fd < 0)
		{
			resp_arg[i].hd = req->handle;
			resp_arg[i].fd = fd;
			return;
		}
	}
}

static esp_err_t setup_handler(httpd_req_t *req)
{
	setup_dev(req->uri);

	// send index.html
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, index_start, index_end - index_start);

    return ESP_OK;
}

static void setup_dev(const char* setup)
{
	const char* cmd[]={"time", "temp" , "pres" , "humi"};
	chart_bitfield = 0;

	extern TimerHandle_t bmeTimer;

	for (uint8_t i = 0; i < LAST_CMD; i++)
	{
		char * ret_pt = strstr(setup, cmd[i]);
		if (NULL != ret_pt)
		{
			switch ((web_cmd_t)i)
			{
			case TIME:
			{
				int val = atoi(ret_pt + sizeof("time"));
				if (val > 0)
					xTimerChangePeriod(bmeTimer, val * 1000LL / portTICK_PERIOD_MS, 10);
				printf("Setup base time measure: %d[sek]\n", val);
				break;
			}
			case TEMP:
			case PRES:
			case HUMI:
				chart_bitfield |= (1 << i);
				printf("Activated chart: %s\n", cmd[i]);
				break;
			default:
				break;
			}
		}
	}
}

static esp_err_t event_handler(httpd_req_t *req)
{
	int fd = httpd_req_to_sockfd(req);
    if (fd < 0) {
    	return ESP_FAIL;
    }
    add_socketfd_to_table(fd, req);
    httpd_default_send(req->handle, fd, hdr, sizeof(hdr), 0);
	vTaskResume(send_task_h);

    return ESP_OK;
}

/* Our URI handler function to be called during GET /uri request */
static esp_err_t get_handler(httpd_req_t *req)
{
	const char main_js[] = "/main.js";
	const char *file_start;
	int file_size;

    /* Get handle to embedded file */

	if (memcmp((char*)req->uri, main_js, sizeof(main_js)) == 0)
	{
		printf("get main.js\n");
	    file_size = main_end - main_start;
	    file_start = main_start;
	    httpd_resp_set_type(req, "application/javascript");
	}
	else
	{
		printf("get:%s\n", (char*)req->uri);

	    file_size = index_end - index_start;
	    file_start = index_start;
	    httpd_resp_set_type(req, "text/html");
	}
    httpd_resp_send(req, file_start, file_size);
    return ESP_OK;
}

/* Our URI handler function to be called during POST /uri request */
static esp_err_t post_handler(httpd_req_t *req)
{
    /* Destination buffer for content of HTTP POST request.
     * httpd_req_recv() accepts char* only, but content could
     * as well be any binary data (needs type casting).
     * In case of string data, null termination will be absent, and
     * content length would give length of string */
    char content[100];

    /* Truncate if content length larger than the buffer */
    size_t recv_size = MIN(req->content_len, sizeof(content));

    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) {  /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        return ESP_FAIL;
    }

    /* Send a simple response */
    const char resp[] = "BME280 server POST Response";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}
