#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
//#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"

#include "bme280-interface.h"
#include "web_server.h"
#include "mdns.h"			//// run -> idf.py add-dependency "espressif/mdns^1.2.1"
#include "esp_sntp.h"

// security private file
#include "security_ap_data.h"

static void vBmeTimerCback( TimerHandle_t pxTimer );
static void start_mdns_service();
static void current_time_print();
static void time_sync_notification_cb(struct timeval *tv);

struct bme280_data bme280_data;
TimerHandle_t bmeTimer;

static void on_got_ip(void *arg, esp_event_base_t event_base,
                      int32_t event_id, void *event_data)
{
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    printf("IP address:%d:%d:%d:%d\n",IP2STR(&event->ip_info.ip));
}

void app_main(void)
{
    nvs_flash_init();
	ESP_ERROR_CHECK(esp_netif_init());

	ESP_ERROR_CHECK(esp_event_loop_create_default());
	esp_netif_create_default_wifi_sta();

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip, NULL));

	wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_EXAMPLE_WIFI_SSID,
            .password = CONFIG_EXAMPLE_WIFI_PASSWORD,
            .scan_method = WIFI_ALL_CHANNEL_SCAN,
			.threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    printf("Connecting to %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    esp_wifi_connect();

    // set host name
//	tcpip_adapter_set_hostname(WIFI_IF_STA, "KTP-ESP32"); <- deprecated

    // start web server
    httpd_handle_t web_server_h = start_webserver();
    // unused variable
    (void) web_server_h;

    // start mDNS service
    start_mdns_service();

    // synchronize time
    setenv("TZ", "CET-1", 1);
    tzset();

    esp_sntp_setoperatingmode(SNTP_OPMODE_POLL);
    esp_sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    esp_sntp_init();

    // init bme280
    bme280_setup();
    // init bme280 timer
    bmeTimer = xTimerCreate("Timer",			       // Just a text name, not used by the kernel.
						   	1000 / portTICK_PERIOD_MS, // The timer period in ticks.
							pdTRUE,        			   // The timers will auto-reload themselves when they expire.
							NULL,		   			   // Assign each timer a unique id equal to its array index.
							vBmeTimerCback 			   // Each timer calls the same callback when it expires.
    );
}

static void vBmeTimerCback( TimerHandle_t pxTimer )
{
	struct timeval tv;
	uint64_t time_ms;

	 bme280_measure(&bme280_data);
	 current_time_print();
	 bme280_printData(&bme280_data);

	 gettimeofday(&tv, NULL);
	 time_ms = tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;

	 for (uint8_t i = 0; i < CHAR_BIT; i++)
	 {
		 extern uint8_t chart_bitfield;
		 float chart_f = 0;

		 if (chart_bitfield & (1 << i))
		 {
			 switch ((web_cmd_t)i)
			 {
			 case TEMP:
				 chart_f = bme280_data.temperature;
				 break;
			 case PRES:
				 chart_f = bme280_data.pressure / 100;
				 break;
			 case HUMI:
				 chart_f = bme280_data.humidity;
				 break;
			 default:
				 break;
			 }
			 web_server_send_event(i, time_ms, chart_f);
		 }
	 }
}

static void start_mdns_service()
{
    //initialize mDNS service
    esp_err_t err = mdns_init();
    if (err) {
        printf("MDNS Init failed: %d\n", err);
        return;
    }

    //set hostname
    mdns_hostname_set("bme280");
    //set default instance
    mdns_instance_name_set("bme280 web server");
}

static void current_time_print()
{
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	printf(asctime(timeinfo));
}

static void time_sync_notification_cb(struct timeval *tv)
{
    printf("Notification of a time synchronization event\n");
    esp_sntp_stop();
    if( xTimerStart( bmeTimer, 0 ) != pdPASS )
    {
    	// The timer could not be set into the Active state.
    }
}

